# A very simple table project...

from flask import Flask,render_template,request, redirect,url_for
from pony.orm import Database, Required, PrimaryKey, select, db_session
'''
db = Database()

class Todo(db.Entity):
    id = PrimaryKey(int, auto=True)
    task_name = Required(str)
    duration = Required(int)
    location = Required(int)

db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)
'''

app = Flask(__name__) #app variable instace of flask application
db = Database() #created object instance

class Product(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    quantity = Required(int)
    price = Required(int)

db.bind(provider='sqlite', filename='productdb', create_db=True)
db.generate_mapping(create_tables=True)



class Person:

    def __init__(self, name,city,proffesion):
        self.name=name
        self.city = city
        self.proffesion= proffesion



@app.route('/') #route == /index
def form():
    return render_template('form.html')


@app.route('/table') #route == /index
def table():
    people=[Person('Bill', 'Lushnje' , 'Manager'),
            Person('Steve', 'Diber' , 'Manager'),
            Person('Tim', 'Vlore' , 'Manager'),
            Person('Marko', 'Tropoja' , 'Manager'),
            Person('Elon', 'Durres' , 'Manager')]

    return render_template('table.html', PEOPLE=people)

#things = [{'task_name': 'task', 'duration': '10:30', 'location': 'home'},
            #    {'task_name': 'ndgfh', 'duration': '53:34', 'location': 'gr'}] do not reload

#CRUD create read update delete
#db browser download to seee what is happening with database




@app.route('/products',methods=['GET' , 'POST']) #route == /index
@db_session
def products():
    if request.method == 'GET':

        searched_term=request.args.get('product','')
        find = list(select(t for t in Product if searched_term.upper() in t.name.upper()))
        return render_template('product_table.html',LIST = find)

    elif request.method == 'POST':
        Product(name = request.form.get('name'),quantity = request.form.get('quantity'),price = request.form.get('price'))
        return redirect(url_for('products'))

        #return render_template('products_table.html',LIST = find)
        #new_things = list(select(t for t in Prdouct)) #return query object / all the rows
        #return redirect(url_for('func'))

@app.route('/delete/<id>', methods=['GET','POST'])
@db_session
def delete(id):
    if Product[id]:
        Product[id].delete()
        return redirect(url_for('products'))

'''
    Product[id].delete() #issue - will return error second time is ussed
    return redirect(url_for('products'))

    to_delete = Product.get(id=id)
    if to_delete:\
        Product[id].delete()
'''


@app.route('/products/<id>', methods=['GET' , 'POST'])
@db_session
def update(id):
        #Product[id].update()
        if request.method == 'GET':
            if Product[id]:
                return render_template('individual.html',LIST = Product[id])
        elif request.method == 'POST':
            if Product[id]:
                n = request.form.get('name')
                q= request.form.get('quantity')
                p= request.form.get('price')
                Product[id].set(name=n, quantity=q, price=p)

                return redirect(url_for('products'))

            return 'This product does not exist'


if __name__ == '__main__':
    app.run(threaded=True, port=5000)

'''
                if select(t for t in Product if t.id==id):
                    Product[id].name=request.form.get('name')
                    Product[id].quantity=request.form.get('quantity')
                    Product[id].price=request.form.get('price')
                return redirect(url_for('products'))
        #return render_template('individual.html')
        #return redirect(url_for('products'))
'''


